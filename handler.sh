#! /bin/bash
apt-get update
apt-get install jq -y
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
apt-get install unzip -y
unzip awscliv2.zip
apt install git -y
./aws/install

aws configure set aws_access_key_id "${ORIGINAL_AWS_ACCESS_KEY_ID}"
aws configure set aws_secret_access_key "${ORIGINAL_AWS_SECRET_ACCESS_KEY}"
AWS_DEFAULT_REGION="eu-west-1"
unset  AWS_SESSION_TOKEN
temp_role=$(aws sts assume-role --role-arn ${SP_MASTER_AWS_ROLE_ARN} --role-session-name "kampus-master-role")
export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq .Credentials.AccessKeyId | xargs)
export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq .Credentials.SecretAccessKey | xargs)
export AWS_SESSION_TOKEN=$(echo $temp_role | jq .Credentials.SessionToken | xargs)
#aws sts assume-role --role-arn ${TARGET_ACCOUNT_ROLE} --role-session-name test | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"'
#aws cloudformation deploy --template static-site.yml --stack-name kampus-storybook-static-site --parameter-overrides DomainName=storybook.kampusdev.sanomapro.fi Application=KAMPUS-COMPONENT-LIBRARY --region eu-west-1 --tags APPLICATION=KAMPUS --capabilities CAPABILITY_NAMED_IAM

chmod +x build-and-tag.sh
./build-and-tag.sh
#cloudfrontDNS=$(aws cloudfront list-distributions --query "DistributionList.Items[].{DomainName: DomainName, OriginDomainName: Origins.Items[0].DomainName}[?contains(OriginDomainName, 'storybook.kampusdev.sanomapro.fi')] | [0].DomainName " --region eu-west-1 --output text)
#echo "Resolved cloudfrontDNS: $cloudfrontDNS"
#unassume_role

#export AWS_ACCESS_KEY_ID=$ORIGINAL_AWS_ACCESS_KEY_ID
#export AWS_SECRET_ACCESS_KEY=$ORIGINAL_AWS_SECRET_ACCESS_KEY
#export AWS_SESSION_TOKEN=

#eval $(aws sts assume-role --role-arn arn:aws:iam::399292214452:role/SERVICE_ROLE --role-session-name test | jq -r '.Credentials | "export AWS_ACCESS_KEY_ID=\(.AccessKeyId)\nexport AWS_SECRET_ACCESS_KEY=\(.SecretAccessKey)\nexport AWS_SESSION_TOKEN=\(.SessionToken)\n"')
#echo "Resolved cloudfrontDNS: $cloudfrontDNS"
#echo "Deploying storybook DNS"
#aws cloudformation deploy --template storybook-dns.yml --stack-name storybook-dns --parameter-overrides DomainName=$cloudfrontDNS Application=KAMPUS-COMPONENT-LIBRARY --region eu-west-1 --tags APPLICATION=KAMPUS --capabilities CAPABILITY_NAMED_IAM
