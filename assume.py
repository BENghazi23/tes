import slack
import os

#os.environ['SLACK_TOKEN'] = 'SLACK_TOKEN'

# init slack client with access token
slack_token = os.environ['SLACK_TOKEN']
client = slack.WebClient(token=slack_token)

# upload file
response = client.files_upload(
    file='handler.js',
    initial_comment='This space ship needs some repairs I think...',
    channels='virus-scanner'
)
assert response['ok']
slack_file = response['file']