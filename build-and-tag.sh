#!/bin/bash
set -e
# Will build and dockerize the project, and push the image to ECR
# Reads two environment variables:
# TAG_RELEASE - whether the build should be tag in version control
# TAG - the tag that will be set to Docker image (and in version control, if TAG_RELEASE se to true)
ARTIFACT_ID="$ARTIFACT_ID"
#AWS_ECR_REPOSITORY_BASE_PATH="399292214452.dkr.ecr.eu-west-1.amazonaws.com/disco"
#ARTIFACT_ID="disco-content-publish"
# The Maven project version stays the same until we get the Maven release working properly.
PROJECT_VERSION="0.1-SNAPSHOT"

# If no specific tag set, use timestamp ("yyyyMMdd.HHmm")
if [ -z "$BITBUCKET_BUILD_NUMBER" ]; then
    TAG=$(echo $(date "+%Y%m%d.%H%M"))
fi

# Check that we don't already have a release with this tag name
tag_exists=$(git ls-remote --tags | grep refs | cut -d '/' -f3 | grep -Fx $BITBUCKET_BUILD_NUMBER | wc -l | xargs)
if [[ $bitbucket_build_number_exists -ne 0 ]]; then
     echo "Tag $BITBUCKET_BUILD_NUMBER exists, will not continue!"
    exit 1
fi

# Login to ECR
aws ecr get-login-password \
    --region eu-west-1 \
| docker login \
    --username AWS \
    --password-stdin  399292214452.dkr.ecr.eu-west-1.amazonaws.com

docker build --build-arg BITBUCKET_REPO_SLUG="${BITBUCKET_REPO_SLUG}" \
             -f Dockerfile.ecs -t $BITBUCKET_BUILD_NUMBER .
docker tag $ARTIFACT_ID:$BITBUCKET_BUILD_NUMBER $AWS_ECR_REPOSITORY_BASE_PATH/$ARTIFACT_ID:$BITBUCKET_BUILD_NUMBER
docker tag $ARTIFACT_ID:$BITBUCKET_BUILD_NUMBER $AWS_ECR_REPOSITORY_BASE_PATH/$ARTIFACT_ID:latest